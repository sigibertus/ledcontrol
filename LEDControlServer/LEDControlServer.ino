#include <WiFi.h>
#include <LiquidCrystal_I2C.h> 
#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
#include <avr/power.h>
#endif
#define PIN T9 // Hier wird angegeben, an welchem digitalen Pin die WS2812 LEDs bzw. NeoPixel angeschlossen sind
#define NUMPIXELS 1 // Hier wird die Anzahl der angeschlossenen WS2812 LEDs bzw. NeoPixel angegeben

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

int lcdColumns = 16;
int lcdRows = 2;

int delayval = 50;

LiquidCrystal_I2C lcd(0x27, lcdColumns, lcdRows);  
const char* ssid = "LedControlNetwork";
const char* password = "LedControl";

WiFiServer wifiServer(1234);

void setup() {
  
   pixels.begin();
  // initialize LCD
  lcd.init();
  // turn on LCD backlight                      
  lcd.backlight();

  // put your setup code here, to run once:
  Serial.begin(115200);
 
  delay(1000);
 
  WiFi.begin(ssid, password);
 
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting to WiFi..");
  }
  
  Serial.println("Connected to the WiFi network");
  Serial.println(WiFi.localIP()); // print this to an lcd
 
  wifiServer.begin();

  String IPaddress = ip2Str(WiFi.localIP());
  writeOnLCD(IPaddress);
}

String ip2Str(IPAddress ip){
  String s="";
  for (int i=0; i<4; i++) {
    s += i  ? "." + String(ip[i]) : String(ip[i]);
  }
  return s;
}

void loop() {
    
  // put your main code here, to run repeatedly:
  WiFiClient client = wifiServer.available();
  if(client) {
    String command = "";
    while (client.connected()) {
      while (client.available()>0) {
        char c = client.read();
        if(c != ','){
          Serial.print(c);
          command += c;
        } else{
          controlLed(command);
          
          String strRed = split(command, '.',0);
          String strGreen = split(command, '.',1);
          String strBlue = split(command, '.',2);
          
          changeColor(strRed.toInt(), strGreen.toInt(), strBlue.toInt());
        }
      }
 
      delay(10);
    }
    client.stop();
    Serial.println("Client disconnected");  
  }
}

String split(String s, char parser, int index) {
  String rs="";
  int parserIndex = index;
  int parserCnt=0;
  int rFromIndex=0, rToIndex=-1;
  while (index >= parserCnt) {
    rFromIndex = rToIndex+1;
    rToIndex = s.indexOf(parser,rFromIndex);
    if (index == parserCnt) {
      if (rToIndex == 0 || rToIndex == -1) return "";
      return s.substring(rFromIndex,rToIndex);
    } else parserCnt++;
  }
  return rs;
}

void controlLed(String ledCommand){
  Serial.println(ledCommand);
  writeOnSecondLineLCD(ledCommand);
}

void writeOnLCD(String message){

  lcd.setCursor(0, 0);
  // print message
  Serial.println("message on lcd screen");
  lcd.print(message);
}

void writeOnSecondLineLCD(String message){
  lcd.setCursor(0, 1);
  lcd.print("                  ");
  lcd.setCursor(0, 1);
  // print message
  Serial.println("message on lcd screen");
  lcd.print(message);
}

void changeColor(int Red, int Green, int Blue){
  for(int i=0;i<NUMPIXELS;i++){
    pixels.setPixelColor(i, pixels.Color(255,255,255)); // Moderately bright green color.
    pixels.show(); // This sends the updated pixel color to the hardware.
    delay(delayval); // Delay for a period of time (in milliseconds).
  }
}
