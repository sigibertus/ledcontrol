﻿using System;
using SQLite;
using LedControl.Droid;
using System.IO;

[assembly: Xamarin.Forms.Dependency(typeof(DatabaseConnection_Android))]
namespace LedControl.Droid {
    public class DatabaseConnection_Android : IDataBaseConnection{
            
        public SQLiteConnection DBConnection() {
            var dbName = "deviceDB.db3";
            var path = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), dbName);
            return new SQLiteConnection(path);

        }
    }
}
