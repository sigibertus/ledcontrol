﻿using System.IO;
using SQLite;
using LedControl.iOS;


[assembly: Xamarin.Forms.Dependency(typeof(DatabaseConnection_IOS))]
namespace LedControl.iOS {
    public class DatabaseConnection_IOS : IDataBaseConnection {

        public SQLiteConnection DBConnection() {
            var dbName = "deviceDB.db3";
            string personalFolder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            string libraryFolder = Path.Combine(personalFolder, "..", "Library");
            var path = Path.Combine(libraryFolder, dbName);
            return new SQLiteConnection(path);
        }
    }
}