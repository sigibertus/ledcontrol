﻿using System;
using System.Threading;
using System.Collections.Generic;
using SQLite;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace LedControl {
    public partial class Devices : ContentPage {
        private SQLiteConnection db;    // creating the reference for the database
        public static ObservableCollection<controller> controllerTable;   // for storing the data of the table
        private List<Switch> switches = new List<Switch>(); // switchlist that is avaible in the whole class so the switchToggle method and the loadDevice Method can access the list
        private List<Button> buttons = new List<Button>(); //   buttonlist that is avaible in the whole class so the ButtonClick method and the loadDevice Method can access the list

        public Devices() {
            InitializeComponent();

            db = DependencyService.Get<IDataBaseConnection>().DBConnection();   // get database connection
            db.CreateTable<controller>(); // create the controller table, does not create a new table if the table already exists
            controllerTable = new ObservableCollection<controller>(db.Table<controller>()); //get the data of the table

            entryIPaddress.Text = "";   // make both string not equal to null
            entryName.Text = "";        // usefull for later check

            initializePopUps(); // initializing a popup for cahnging a Device
            loadDevices();  // initialize show for saved devices
            BtnAddDevice.Clicked += OnAddDeviceClick; // add an event fo the button click
        }

        private void initializePopUps() {
            StackLayout stack = new StackLayout();
            stack.Children.Add(new Entry { Placeholder = "IPaddress" });
            stack.Children.Add(new Entry { Placeholder = "Name" });
            stack.Children.Add(new Button { Text = "Change" });
        }


        private void loadDevices() {
            Grid grid = new Grid(); // creating the grid
            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(140) });   //  adds
            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(100) });   //  three
            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(60) });   //  colums

            switches.Clear(); // clear switch list so old switches dont get doubled 
            int counter = 0;    // creating a counter for position and for the switches
            foreach (controller device in controllerTable) {
                // TODO add an edit and an delete button
                grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(1, GridUnitType.Star) });   // adds one row

                StackLayout stack = new StackLayout { };    // creating a stacklayout for a better locking layout
                stack.Children.Add(new Label { Text = device.name });       // filling the stacklayout with the device name
                stack.Children.Add(new Label { Text = device.IPaddress });  // and the device IPaddress
                grid.Children.Add(stack, 0, counter);   // adding to stacklayout to the grid

                buttons.Add(new Button { Text = "Delete" });
                grid.Children.Add(buttons[counter], 1, counter);
                buttons[counter].Clicked += OnDeleteClick;

                switches.Add(new Switch { IsToggled = device.isOn , HorizontalOptions = LayoutOptions.EndAndExpand, VerticalOptions = LayoutOptions.Center }); // adding a switch to the switches list
                grid.Children.Add(switches[counter], 2, counter);   // adding that switch to the grid
                switches[counter].Toggled += ToggleButton;  // add an event for the switch toggle
                counter++;  // counting up the pointer for position and for switches
            }

            stLayout.Children.Clear();  // clearing old objects so it does not get shown after reload
            stLayout.Children.Add(grid);    // loading all the objects            
        }

        private void ToggleButton(object sender, ToggledEventArgs e) {
            int _counter = 0; // counter to keep track of the object that the switch is for
            foreach (Switch tempSwitch in switches) {   // looping through all siwtches
                if (tempSwitch.Equals(sender)) {    // finding the sender switch button
                    controllerTable[_counter].isOn = tempSwitch.IsToggled;   // changing the isOn value of object that the switch is made for
                    break;  // stoping the loop after finden the right switch
                }
                _counter++; // counting up to keep track of the right
            }
            UpdateDatabase();
        }

        private void OnDeleteClick(object sender, EventArgs e) {
            int _counter = 0; // counter to keep track of the object that the switch is for
            foreach (Button tempButton in buttons) {   // looping through all siwtches
                if (tempButton.Equals(sender)) {    // finding the sender button
                    db.Delete(controllerTable[_counter]);
                }
                _counter++; // counting up to keep track of the right controller
            }
            controllerTable = new ObservableCollection<controller>(db.Table<controller>()); // updating the table so the delete works live

            loadDevices();
            UpdateDatabase();
        }

        private void OnAddDeviceClick(object sender, EventArgs e) {
            if(entryIPaddress.Text != "" && entryName.Text != "") {
                controllerTable.Add(new controller(entryName.Text, entryIPaddress.Text, true)); // adding the new controller
                entryIPaddress.Text = "";   // resetting the
                entryName.Text = "";        // entry fields
                loadDevices();              // reloading the devices so the new device gets shown
                UpdateDatabase();           // updating the database / adding the new device
            }
        }

        private void UpdateDatabase() {
            foreach(controller device in controllerTable) { // looping throught the controllerTable for updating it in the database
                if (device.Id != 0) {   // check if it is already in the database 
                    db.Update(device);  // update it, if its already in the database
                } else {
                    db.Insert(device);  // insert if it is not in the database
                }
            }
        }
    }
}
