﻿using System;
using SQLite;
using System.Collections.ObjectModel;
using System.Threading;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Net.Sockets;
using System.Net.NetworkInformation;
using Xamarin.Forms;

namespace LedControl
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        const int modifier = 15;

        private double douRed;
        private double douGreen;
        private double douBlue;

        Thread effectThread;
        Thread dataSender;
        private SQLiteConnection db;
        private ObservableCollection<controller> controllerTable;   // for storing the data of the table
        private bool currenColorIsBig = false;
        Random rnd = new Random(DateTime.Now.Millisecond);
        private bool isFading;

        public MainPage(){
            InitializeComponent();

            //var autoEvent = new AutoResetEvent(false);
            //t = new Timer(checkConnection, autoEvent, 10000, 10000);

            db = DependencyService.Get<IDataBaseConnection>().DBConnection();   // get database connection
            db.CreateTable<controller>(); // create the controller table, does not create a new table if the table already exists
            controllerTable = new ObservableCollection<controller>(db.Table<controller>()); //get the data of the table

            //changeColor();
            setupSlider();
            setupButtons();
        }


        private void setupSlider() {
            // giving the slider an changed event so the color updates
            sliderRed.ValueChanged += sliderValueChanged;
            sliderGreen.ValueChanged += sliderValueChanged;
            sliderBlue.ValueChanged += sliderValueChanged;

            sliderFadeSpeed.ValueChanged += sliderFadeSpeedValueChanged;
        }

        private void setupButtons(){
            // giving buttons a click event
            // colored Buttons
            BtnLightBlue.Clicked += OnColorButtonClick;
            BtnBlue.Clicked += OnColorButtonClick;
            BtnDarkBlue.Clicked += OnColorButtonClick;
            BtnDarkestBlue.Clicked += OnColorButtonClick;
            BtnLightRed.Clicked += OnColorButtonClick;
            BtnRed.Clicked += OnColorButtonClick;
            BtnDarkRed.Clicked += OnColorButtonClick;
            BtnDarkestRed.Clicked += OnColorButtonClick;
            BtnLightGreen.Clicked += OnColorButtonClick;
            BtnGreen.Clicked += OnColorButtonClick;
            BtnDarkGreen.Clicked += OnColorButtonClick;
            BtnDarkestGreen.Clicked += OnColorButtonClick;
            BtnWhite.Clicked += OnColorButtonClick;
            BtnPink.Clicked += OnColorButtonClick;
            BtnMagenta.Clicked += OnColorButtonClick;
            BtnYellow.Clicked += OnColorButtonClick;

            // special effect Buttons
            BtnFadeOne.Clicked += FadeOneClick;
            BtnFadeTwo.Clicked += FadeTwoClick;
            BtnFadeThree.Clicked += FadeThreeClick;
            BtnFadeFour.Clicked += FadeFourClick;
            BtnSmootherFadeOne.Clicked += FadeSmootherOneClick;
            BtnSmootherFadeTwo.Clicked += FadeSmootherTwoClick;
            BtnSmootherFadeThree.Clicked += FadeSmootherThreeClick;
            BtnSmootherFadeFour.Clicked += FadeSmootherFourClick;
            BtnBrighter.Clicked += OnBrighterClick;
            BtnDarker.Clicked += OnDarkerClick;

            BtnCurrentColor.Clicked += OnCurrentColorClick;
           
        }

        private void sliderFadeSpeedValueChanged(object sender, EventArgs e) =>
            lblFadeSpeed.Text = "Value: " +  Convert.ToInt32(sliderFadeSpeed.Value).ToString();
        

        private void changeColor(){
            //System.Drawing.Color currentColor = System.Drawing.Color.FromArgb(255 , (int) sliderRed.Value, (int) sliderGreen.Value, (int)sliderBlue.Value); // calculating the color by argb
            if (douRed > 255) douRed = 255;
            if (douGreen > 255) douGreen = 255;
            if (douBlue > 255) douBlue = 255;

            System.Drawing.Color currentColor = System.Drawing.Color.FromArgb(255, (int)douRed, (int)douGreen, (int)douBlue); // calculating the color by argb

            try {
                BtnCurrentColor.BackgroundColor = currentColor;   // updating the color of the box
            } catch { } 
            stopDateSenderThread();
            dataSender = new Thread(new ThreadStart(sendData));
            dataSender.Start();
       
        }

        private void stopDateSenderThread() {
            if (dataSender != null && dataSender.IsAlive) {  // checking if the thread is on
                dataSender.Abort();   //stopping the thread
            }
        }

        public void sendData() {
            foreach (controller tempController in controllerTable) {// loop trough Devices
                if (tempController.isOn) {  // if a device is turned on
                    //TODO send a string of colorData to that Device
                    if (isFading) {
                        SendMessage(tempController.IPaddress, String.Format("Fade.{0}.{1}.{2}", (int)douRed, (int)douGreen, (int)douBlue));
                    } else {
                        SendMessage(tempController.IPaddress, String.Format("Color.{0}.{1}.{2}", (int)douRed, (int)douGreen, (int)douBlue));
                    }
                }
            }            
        }

        // gets the connection to an arduino and sends a message also returns false if something fails
        private void SendMessage(string server, string message) {
            try {
                UdpClient udpClient = new UdpClient(1234);

                udpClient.Connect(server, 1234);

                // Sends a message to the host to which you have connected.
                byte[] sendBytes = Encoding.ASCII.GetBytes(message);

                udpClient.Send(sendBytes, sendBytes.Length);
            } catch { }
        }

        private void OnCurrentColorClick(object sender, EventArgs e) {
            if (!currenColorIsBig) {
                BtnCurrentColor.HeightRequest = Application.Current.MainPage.Height;
            } else {
                BtnCurrentColor.HeightRequest = 50;
            }
            currenColorIsBig = !currenColorIsBig; 
        }

        private void OnBrighterClick(object sender, EventArgs e) {
            // adding a bit to everything so the color stays and it only goes brighter
            sliderRed.Value += 10;
            sliderGreen.Value += 10;
            sliderBlue.Value += 10;
        }

        private void OnDarkerClick(object sender, EventArgs e) {
            // substracting a bit from everything so the color stays and it only goes darker
            sliderRed.Value -= 10;
            sliderGreen.Value -= 10;
            sliderBlue.Value -= 10;
        }

        private void sliderValueChanged(object sender, EventArgs e){
            // updating the value text and changing the color if the value of one of the color sliders is changed
            lblValueRed.Text = "Value: " + (int) sliderRed.Value;
            lblValueGreen.Text = "Value: " + (int) sliderGreen.Value;
            lblValueBlue.Text = "Value: " + (int) sliderBlue.Value;

            douRed = sliderRed.Value;
            douGreen = sliderGreen.Value;
            douBlue = sliderBlue.Value;

            changeColor();
        }

        private void OnColorButtonClick(object sender, EventArgs e){
            stopEffectThread(); // stopping the thread so the fade effect does not change the color instantly

            Button color = sender as Button;    // converting for color access

            /*sliderRed.Value = color.BackgroundColor.R * 255;    // background color is double between 0 and 1 but a value between 0 and 255 is needed
            sliderGreen.Value = color.BackgroundColor.G * 255;
            sliderBlue.Value = color.BackgroundColor.B * 255;*/

            douRed = color.BackgroundColor.R * 255;
            douGreen = color.BackgroundColor.G * 255;
            douBlue = color.BackgroundColor.B * 255;

            changeColor();
        }

        #region Fadeeffects
        private void stopEffectThread(){
            isFading = false;
            if (effectThread != null && effectThread.IsAlive){  // checking if the thread is on
                effectThread.Abort();   //stopping the thread
            }
        }

        private void FadeOneClick(object sender, EventArgs e){
            stopEffectThread(); // stopping the thread so the threads dont overlap
            effectThread = new Thread(new ThreadStart(fadeOneEffect));  // getting the effect method in the thread
            effectThread.Start();   // starting the thread
        }

        private void fadeOneEffect(){
            // Device.BeginInvokeOnMainThread(() => {} is needed fo changing something in the main thread
            // setting all values to 0 for the animation
            Device.BeginInvokeOnMainThread(() => {
                douRed = 0;
                douGreen = 0;
                douBlue = 0;
            });
            Thread.Sleep((int) sliderFadeSpeed.Value);
            // starting animation by getting rgb up and down
            while (true){
                for (int i = 0; i < 255; i += modifier) {
                    Device.BeginInvokeOnMainThread(() => {
                        douRed = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
                for (int i = 0; i < 255; i += modifier){
                    Device.BeginInvokeOnMainThread(() => {
                        douGreen = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
                for (int i = 0; i < 255; i += modifier)
                {
                    Device.BeginInvokeOnMainThread(() => {
                        douBlue = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
                for (int i = 255; i > 0; i -= modifier) {
                    Device.BeginInvokeOnMainThread(() => {
                        douRed = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
                for (int i = 255; i > 0; i -= modifier) {
                    Device.BeginInvokeOnMainThread(() => {
                        douGreen = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
                for (int i = 255; i > 0; i -= modifier) {
                    Device.BeginInvokeOnMainThread(() => {
                        douBlue = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
            }
        }

        private void FadeTwoClick(object sender, EventArgs e) {
            stopEffectThread(); // stopping the thread so the threads dont overlap
            effectThread = new Thread(new ThreadStart(fadeTwoEffect));  // getting the effect method in the thread
            effectThread.Start();   // starting the thread
        }

        private void fadeTwoEffect() {
            // Device.BeginInvokeOnMainThread(() => {} is needed fo changing something in the main thread
            // setting all values to 0 for the animation
            Device.BeginInvokeOnMainThread(() => {
                douRed = 0;
                douGreen = 0;
                douBlue = 0;
            });
            // starting animation by getting rgb up and down
            Thread.Sleep((int)sliderFadeSpeed.Value);
            while (true) {
                for (int i = 0; i < 255; i += modifier) {
                    Device.BeginInvokeOnMainThread(() => {
                        douBlue = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
                for (int i = 0; i < 255; i += modifier) {
                    Device.BeginInvokeOnMainThread(() => {
                        douRed = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
                for (int i = 0; i < 255; i += modifier) {
                    Device.BeginInvokeOnMainThread(() => {
                        douGreen = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
                for (int i = 255; i > 0; i -= modifier) {
                    Device.BeginInvokeOnMainThread(() => {
                        douBlue = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
                for (int i = 255; i > 0; i -= modifier) {
                    Device.BeginInvokeOnMainThread(() => {
                        douRed = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
                for (int i = 255; i > 0; i -= modifier) {
                    Device.BeginInvokeOnMainThread(() => {
                        douGreen = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
            }
        }

        private void FadeThreeClick(object sender, EventArgs e) {
            stopEffectThread(); // stopping the thread so the threads dont overlap
            effectThread = new Thread(new ThreadStart(fadeThreeEffect));  // getting the effect method in the thread
            effectThread.Start();   // starting the thread
        }

        private void fadeThreeEffect() {
            // Device.BeginInvokeOnMainThread(() => {} is needed fo changing something in the main thread
            // setting all values to 0 for the animation
            Device.BeginInvokeOnMainThread(() => {
                douRed = 0;
                douGreen = 0;
                douBlue = 0;
            });
            // starting animation by getting rgb up and down
            Thread.Sleep((int)sliderFadeSpeed.Value);
            while (true) {
                for (int i = 0; i < 255; i++) {
                    Device.BeginInvokeOnMainThread(() => {
                        douGreen = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
                for (int i = 0; i < 255; i++) {
                    Device.BeginInvokeOnMainThread(() => {
                        douBlue = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
                for (int i = 0; i < 255; i++) {
                    Device.BeginInvokeOnMainThread(() => {
                        douRed = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
                for (int i = 255; i > 0; i -= modifier) {
                    Device.BeginInvokeOnMainThread(() => {
                        douGreen = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
                for (int i = 255; i > 0; i -= modifier) {
                    Device.BeginInvokeOnMainThread(() => {
                        douBlue = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
                for (int i = 255; i > 0; i -= modifier) {
                    Device.BeginInvokeOnMainThread(() => {
                        douRed = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
            }
        }

        private void FadeFourClick(object sender, EventArgs e) {
            stopEffectThread(); // stopping the thread so the threads dont overlap
            effectThread = new Thread(new ThreadStart(fadeFourEffect));  // getting the effect method in the thread
            effectThread.Start();   // starting the thread
        }

        private void fadeFourEffect() {
            // Device.BeginInvokeOnMainThread(() => {} is needed fo changing something in the main thread
            // setting all values to 0 for the animation
            Device.BeginInvokeOnMainThread(() => {
                douRed = 0;
                douGreen = 0;
                douBlue = 0;
            });
            // starting animation by getting rgb up and down
            Thread.Sleep((int)sliderFadeSpeed.Value);
            while (true) {
                for (int i = 0; i < 255; i++) {
                    Device.BeginInvokeOnMainThread(() => {
                        douGreen = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
                for (int i = 0; i < 255; i++) {
                    Device.BeginInvokeOnMainThread(() => {
                        douRed = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
                for (int i = 0; i < 255; i++) {
                    Device.BeginInvokeOnMainThread(() => {
                        douBlue = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
                for (int i = 255; i > 0; i -= modifier) {
                    Device.BeginInvokeOnMainThread(() => {
                        douGreen = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
                for (int i = 255; i > 0; i -= modifier) {
                    Device.BeginInvokeOnMainThread(() => {
                        douRed = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
                for (int i = 255; i > 0; i -= modifier) {
                    Device.BeginInvokeOnMainThread(() => {
                        douBlue = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
            }
        }

        private void FadeSmootherOneClick(object sender, EventArgs e) {
            stopEffectThread(); // stopping the thread so the threads dont overlap
            isFading = true;
            effectThread = new Thread(new ThreadStart(fadeSmootherOneEffect));  // getting the effect method in the thread
            effectThread.Start();   // starting the thread
        }

        private void fadeSmootherOneEffect() {
            


            int intBlue = rnd.Next(50, 160);
            int intRed = rnd.Next(50, 160);
            int intGreen = rnd.Next(50, 160);
            // Device.BeginInvokeOnMainThread(() => {} is needed fo changing something in the main thread
            // setting all values to the starting value of the animation
            Device.BeginInvokeOnMainThread(() => {
                douRed = intRed;
                douGreen = intGreen;
                douBlue = intBlue;
            });
            // starting animation by getting rgb up and down
            Thread.Sleep((int)sliderFadeSpeed.Value);
            while (true) {
                for (int i = intGreen; i < 255; i += modifier) {
                    Device.BeginInvokeOnMainThread(() => {
                        douGreen = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
                for (int i = intRed; i < 255; i += modifier) {
                    Device.BeginInvokeOnMainThread(() => {
                        douRed = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
                for (int i = intBlue; i < 255; i += modifier) {
                    Device.BeginInvokeOnMainThread(() => {
                        douBlue = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
                for (int i = 255; i > intGreen; i -= modifier) {
                    Device.BeginInvokeOnMainThread(() => {
                        douGreen = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
                for (int i = 255; i > intRed; i -= modifier) {
                    Device.BeginInvokeOnMainThread(() => {
                        douRed = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
                for (int i = 255; i > intBlue; i -= modifier) {
                    Device.BeginInvokeOnMainThread(() => {
                        douBlue = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
            }
        }

        private void FadeSmootherTwoClick(object sender, EventArgs e) {
            stopEffectThread(); // stopping the thread so the threads dont overlap
            effectThread = new Thread(new ThreadStart(fadeSmootherTwoEffect));  // getting the effect method in the thread
            effectThread.Start();   // starting the thread
        }

        private void fadeSmootherTwoEffect() {

            int intRed = rnd.Next(50, 160);
            int intGreen = rnd.Next(50, 160);
            int intBlue = rnd.Next(50, 160);
            // Device.BeginInvokeOnMainThread(() => {} is needed fo changing something in the main thread
            // setting all values to the starting value of the animation
            Device.BeginInvokeOnMainThread(() => {
                douRed = intRed;
                douGreen = intGreen;
                douBlue = intBlue;
            });
            // starting animation by getting rgb up and down
            Thread.Sleep((int)sliderFadeSpeed.Value);
            while (true) {
                for (int i = intRed; i < 255; i += modifier) {
                    Device.BeginInvokeOnMainThread(() => {
                        douRed = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
                for (int i = intGreen; i < 255; i += modifier) {
                    Device.BeginInvokeOnMainThread(() => {
                        douGreen = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
                for (int i = intBlue; i < 255; i += modifier) {
                    Device.BeginInvokeOnMainThread(() => {
                        douBlue = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
                for (int i = 255; i > intRed; i -= modifier) {
                    Device.BeginInvokeOnMainThread(() => {
                        douRed = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
                for (int i = 255; i > intGreen; i -= modifier) {
                    Device.BeginInvokeOnMainThread(() => {
                        douGreen = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
                for (int i = 255; i > intBlue; i -= modifier) {
                    Device.BeginInvokeOnMainThread(() => {
                        douBlue = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
            }
        }

        private void FadeSmootherThreeClick(object sender, EventArgs e) {
            stopEffectThread(); // stopping the thread so the threads dont overlap
            effectThread = new Thread(new ThreadStart(fadeSmootherThreeEffect));  // getting the effect method in the thread
            effectThread.Start();   // starting the thread
        }

        private void fadeSmootherThreeEffect() {
            int intRed = rnd.Next(50,160);
            int intGreen = rnd.Next(50, 160);
            int intBlue = rnd.Next(50, 160);
            // Device.BeginInvokeOnMainThread(() => {} is needed fo changing something in the main thread
            // setting all values to the starting value of the animation
            Device.BeginInvokeOnMainThread(() => {
                douRed = intRed;
                douGreen = intGreen;
                douBlue = intBlue;
            });
            // starting animation by getting rgb up and down
            Thread.Sleep((int)sliderFadeSpeed.Value);
            while (true) {
                for (int i = intRed; i < 255; i += modifier) {
                    Device.BeginInvokeOnMainThread(() => {
                        douBlue = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
                for (int i = intGreen; i < 255; i += modifier) {
                    Device.BeginInvokeOnMainThread(() => {
                        douRed = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
                for (int i = intBlue; i < 255; i += modifier) {
                    Device.BeginInvokeOnMainThread(() => {
                        douGreen = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
                for (int i = 255; i > intRed; i -= modifier) {
                    Device.BeginInvokeOnMainThread(() => {
                        douGreen = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
                for (int i = 255; i > intGreen; i -= modifier) {
                    Device.BeginInvokeOnMainThread(() => {
                        douBlue = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
                for (int i = 255; i > intBlue; i -= modifier) {
                    Device.BeginInvokeOnMainThread(() => {
                        douRed = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
            }
        }

        private void FadeSmootherFourClick(object sender, EventArgs e) {
            stopEffectThread(); // stopping the thread so the threads dont overlap
            effectThread = new Thread(new ThreadStart(fadeSmootherFourEffect));  // getting the effect method in the thread
            effectThread.Start();   // starting the thread
        }

        private void fadeSmootherFourEffect() {
            int intRed = rnd.Next(10, 160);
            int intGreen = rnd.Next(10, 160);
            int intBlue = rnd.Next(10, 160);
            // Device.BeginInvokeOnMainThread(() => {} is needed fo changing something in the main thread
            // setting all values to the starting value of the animation
            Device.BeginInvokeOnMainThread(() => {
                douRed = intRed;
                douGreen = intGreen;
                douBlue = intBlue;
            });
            // starting animation by getting rgb up and down
            Thread.Sleep((int)sliderFadeSpeed.Value);
            while (true) {
                for (int i = intBlue; i < 255; i += modifier) {
                    Device.BeginInvokeOnMainThread(() => {
                        douBlue = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
                for (int i = intGreen; i < 255; i += modifier) {
                    Device.BeginInvokeOnMainThread(() => {
                        douGreen = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
                for (int i = intRed; i < 255; i += modifier) {
                    Device.BeginInvokeOnMainThread(() => {
                        douRed = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
                for (int i = 255; i > intBlue; i -= modifier) {
                    Device.BeginInvokeOnMainThread(() => {
                        douBlue = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
                for (int i = 255; i > intGreen; i -= modifier) {
                    Device.BeginInvokeOnMainThread(() => {
                        douGreen = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
                for (int i = 255; i > intRed; i -= modifier) {
                    Device.BeginInvokeOnMainThread(() => {
                        douRed = i;
                    });
                    changeColor();
                    Thread.Sleep((int)sliderFadeSpeed.Value * modifier);
                }
            }
        }
        #endregion
    }
}
