﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LedControl {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Tabs : TabbedPage {
        public Tabs() {
            InitializeComponent();

            //contentDevices.Content = new Devices();
            contentLed = new MainPage();
            contentDevices = new Devices();
            
        }
    }
}
