﻿using System;
using SQLite;
using System.ComponentModel;
using Xamarin.Forms;

namespace LedControl {
    [Table("controllerTable")]
    public class controller{
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }    // creating an auto incrementing id as the primary key

        public string name { get; set; }    // each property needs a getter and a setter for the database
        public bool isOn { get; set; }
        public string IPaddress { get; set; }

        public controller() { } // basic constructor needed for database

        public controller(string _name, string _IPaddress, bool _isOn) { // overloaded constructor
            name = _name;
            IPaddress = _IPaddress;
            isOn = _isOn;
        }
    }
}
