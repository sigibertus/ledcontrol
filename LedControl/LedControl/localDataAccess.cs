﻿using System;
namespace LedControl {
    public interface IDataBaseConnection {
        SQLite.SQLiteConnection DBConnection();
    }
}
