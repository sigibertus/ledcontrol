﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LedControl {
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class tabbedPage : TabbedPage {
        public tabbedPage() {
            InitializeComponent();
            this.Children.Add(new MainPage()); // creating the tab for the mainpage/colorControlPage    
            this.Children.Add(new Devices());  // creating the tab for the Devicepage
        }
    }
}
