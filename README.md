# LEDControl
This project is about a simple control over multible LEDstripes, who are controlled via the ESP32.
## Setup

### Devices needed

##### ESP32
I did use an ESP32 u can probably use another mikrocontroller too just note that you need to change the PIN for the LED STRIP

##### Micro USB Cable
A Micro USB Cable is needed to power the ESP32 and also to get data on the ESP32

##### Resistors
Resistors are needed for correct data transfer. I did use 470 Ohm for the data resistor

##### Electrolytic Capacitor
A Condensator is needed so the Led's wont get too much power at once

##### WS2812B
These are the LEDstripes i used, other neopixel LEDstripes should be working too, maybe you need to change the arduino code, for that please visit the documentation of your NeoPixels

##### Power Source
An external Power Source with 5 Volt is needed, please dont use the 5 Volts on the ESP32, because the LEDs are using too much power and with to many LEDs, the ESP32 could break

##### Wires
Wires are needed for everything, the data transfer the power, you will need female to male and male to male Wires

##### Breadboard
A breadboard is used to plug the parts together, you can also use other methods to achieve this, such as female to female wires or soldering everything together

#### Examples on Amazon
Here are some example for the parts, but feel free to look out for other ones, especially on parts like resistors.

[ESP32](https://www.amazon.de/AZDelivery-NodeMCU-Development-Nachfolgermodell-ESP8266/dp/B071P98VTG/ref=sr_1_4?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=esp32&qid=1563366736&s=gateway&sr=8-4)

[MICRO USB Cable](https://www.amazon.de/AmazonBasics-7T9MV4-Verbindungskabel-Stecker-Micro-USB-B-Stecker/dp/B07232M876/ref=sr_1_6?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=10A2VUF89X6H1&keywords=micro+usb+kabel&qid=1563366803&s=gateway&sprefix=micro+usb+%2Caps%2C159&sr=8-6)

[Resistors](https://www.amazon.de/Longruner-Resistors-ohm-10M-Assortment-Assorted/dp/B075ZN78JZ/ref=sr_1_4?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=resistors&qid=1563366859&s=gateway&sr=8-4)

[Electrlytic Capacitor](https://www.amazon.de/Elektrolytkondensator-Sortiment-Electrolytic-Capacitors-Acogedor-Elektrolytkondensator-Kit/dp/B07KQ86DMP/ref=sr_1_2_sspa?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=OJE9MAL85TMD&keywords=electrolytic+capacitors&qid=1563366908&s=gateway&sprefix=electrolytic+%2Caps%2C141&sr=8-2-spons&psc=1)

[WS2812B](https://www.amazon.de/BTF-LIGHTING-WS2812B-300LEDs-Streifen-NichtWasserdicht/dp/B01CDTEJBG/ref=sr_1_1_sspa?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=3GXPW1I7KBGVF&keywords=ws2812b&qid=1563366938&s=gateway&sprefix=ws2%2Caps%2C147&sr=8-1-spons&psc=1)

[Power Source](https://www.amazon.de/Voltcraft-SNG-1500-OW-DrNetzteilucker/dp/B00CEVEJ1O/ref=sr_1_2?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=voltcraft+sng-1500-ow&qid=1563367022&s=gateway&sr=8-2)

[Wires](https://www.amazon.de/Female-Female-Male-Female-Male-Male-Steckbr%C3%BCcken-Drahtbr%C3%BCcken-bunt/dp/B01EV70C78/ref=sr_1_1_sspa?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=arduino+wires&qid=1563367063&s=gateway&sr=8-1-spons&psc=1)

[Breadboard](https://www.amazon.de/Elegoo-Breadboard-Solderless-Distribution-Verbindungsbl%C3%B6cke/dp/B01MCRZFE5/ref=sr_1_5?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=2RY12FD8LOAV4&keywords=breadboard&qid=1563367103&s=gateway&sprefix=bread%2Caps%2C145&sr=8-5)

### Setup LED Strip

#### Wiring
U need to Wire everything as shown below

![](Schaltung.png "Logo Title Text 1")

Please note that the PIN for data is PIN32, also all grounds need to be connected to each other, please use a transistor with 470 Ohm, also the Battery represents the power source, so just use what u have, only watch out that your power source has 5 Volts

#### Installing Arduino IDE
U need to download the arduino IDE to get the programm onto the ESP32 go to [this](https://www.arduino.cc/en/main/software) website and download the Arduino for your OS, i will leave the installation for you.

#### Setup the code
You should use the udpControllerNoLCD as the code, just open that with the Arduino IDE.

Because the Connection works over Wifi, you need to change the ssid and the passwort to that of your Wifi, you can alternatively use a hotspot if you want.

Also u can change the Number of Pixels which should light up on your LEDstripe, I recommend you to set this to the number of Leds your strip has.

#### Installing the ESP32
The ESP32 needs to be installed to the Arduino seperatly, for that please follow [this](https://randomnerdtutorials.com/installing-the-esp32-board-in-arduino-ide-windows-instructions/) tutorial.

#### Running the ESP32
To run the ESP32 u need to chose the Board in the Arduino Software under Tools -> Board, for my Board it was the FireBeetle-ESP32, this can be different when using another board, also u need to set the com Port, for this you can try testing around, if none work, you could need a USBtoUART driver u can download that [here](https://www.silabs.com/products/development-tools/software/usb-to-uart-bridge-vcp-drivers)

After that u can hit the Button with the arrow to get the Programm onto the ESP32.

#### Getting the IP Address
After getting the programm onto the ESP32, you can view the IPAddress by Opening the Serial Monitor when you start the ESP32, by Tools->Seriel Monitor in the Arduino IDE, the IP Address will be displayed there, and it will stay the same after restarting the mikrocontroller.

### Phone Setup
Just install the App, add the mikrocontroller, in the devices tab with the IPAddress of the mikrocontroller, you can freely choose the name
