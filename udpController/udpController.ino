
#include <SPI.h>
#include <WiFi.h>
#include <WiFiUdp.h>
#include <WiFi.h>
#include <LiquidCrystal_I2C.h>
#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
#include <avr/power.h>
#endif
#define PIN 32 // Hier wird angegeben, an welchem digitalen Pin die WS2812 LEDs bzw. NeoPixel angeschlossen sind
#define NUMPIXELS 30 // Hier wird die Anzahl der angeschlossenen WS2812 LEDs bzw. NeoPixel angegeben

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

int lcdColumns = 16;
int lcdRows = 2;

int delayval = 70;

LiquidCrystal_I2C lcd(0x27, lcdColumns, lcdRows);

int status = WL_IDLE_STATUS;
char ssid[] = "LedControlNetwork"; //  your network SSID (name)
char pass[] = "LedControl";    // your network password (use for WPA, or use as key for WEP)


int keyIndex = 0;            // your network key Index number (needed only for WEP)

unsigned int localPort = 1234;      // local port to listen on

char packetBuffer[255]; //buffer to hold incoming packet

WiFiUDP Udp;

String lastPackage = "";

void setup() {

  pixels.begin();
  // initialize LCD
  lcd.init();
  // turn on LCD backlight
  lcd.backlight();

  //Initialize serial and wait for port to open:
  Serial.begin(9600);

  // attempt to connect to Wifi network:
  while (status != WL_CONNECTED) {
    Serial.print("Attempting to connect to SSID: ");
    Serial.println(ssid);
    // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
    status = WiFi.begin(ssid, pass);
    Serial.println(status);
    // wait 10 seconds for connection:
    delay(10000);
  }
  Serial.println("Connected to wifi");
  printWifiStatus();

  Serial.println("\nStarting connection to server...");
  // if you get a connection, report back via serial:
  Udp.begin(localPort);
  String IPaddress = ip2Str(WiFi.localIP());
  writeOnLCD(IPaddress);
}

void loop() {

  // if there's data available, read a packet
  int packetSize = Udp.parsePacket();
  if (packetSize) {
    Serial.print("Received packet of size ");
    Serial.println(packetSize);
    Serial.print("From ");
    IPAddress remoteIp = Udp.remoteIP();
    Serial.print(remoteIp);
    Serial.print(", port ");
    Serial.println(Udp.remotePort());

    // read the packet into packetBufffer
    int len = Udp.read(packetBuffer, 255);
    if (len > 0) {
        packetBuffer[len] = 0;
        Serial.println("LastPackage: " + lastPackage);
      if(lastPackage != packetBuffer){
        lastPackage = packetBuffer; 
        Serial.println("Contents:");
        Serial.println(packetBuffer);
        controlLed(packetBuffer);
        char delimiter[] = ".";
    
        char cRed[3] = "  ";
        char cGreen[3] = "  ";
        char cBlue[3] = "  ";
    
        strtok(packetBuffer, delimiter);
        strcpy(cRed, strtok(NULL, delimiter));
        int iRed = atoi(cRed);
        strcpy(cGreen, strtok(NULL, delimiter));
        int iGreen = atoi(cGreen);
        strcpy(cBlue, strtok(NULL, delimiter));
        int iBlue = atoi(cBlue);
    
         
        changeColor(iRed, iGreen, iBlue);
      }
    }
  }
}


void printWifiStatus() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your WiFi shield's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}

void controlLed(String ledCommand) {
  Serial.println(ledCommand);
  writeOnSecondLineLCD(ledCommand);
}

void writeOnLCD(String message) {

  lcd.setCursor(0, 0);
  // print message
  Serial.println("message on lcd screen");
  lcd.print(message);
}

void writeOnSecondLineLCD(String message) {
  lcd.setCursor(0, 1);
  lcd.print("                  ");
  lcd.setCursor(0, 1);
  // print message
  Serial.println("message on lcd screen");
  lcd.print(message);
}

void changeColor(int Red, int Green, int Blue) {
  for (int i = 0; i < NUMPIXELS; i++) {
    //Serial.println("Pixel: " + String(i) + " R: " + String(Red) + " G: " + String(Green) + " B: " + String(Blue));
    pixels.setPixelColor(i, pixels.Color(Red, Green, Blue)); // Moderately bright green color.
     pixels.show(); // This sends the updated pixel color to the hardware.
    delay(delayval); // Delay for a period of time (in milliseconds).
  }

}

String ip2Str(IPAddress ip) {
  String s = "";
  for (int i = 0; i < 4; i++) {
    s += i  ? "." + String(ip[i]) : String(ip[i]);
  }
  return s;
}
