
#include <SPI.h>
#include <WiFi.h>
#include <WiFiUdp.h>x
#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
#include <avr/power.h>
#endif
#define PIN 32 // Hier wird angegeben, an welchem digitalen Pin die WS2812 LEDs bzw. NeoPixel angeschlossen sind
#define NUMPIXELS 60 // Hier wird die Anzahl der angeschlossenen WS2812 LEDs bzw. NeoPixel angegeben

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

int delayval = 10;

int status = WL_IDLE_STATUS;
char ssid[] = "LedControlNetwork"; //  your network SSID (name)
char pass[] = "LedControl";    // your network password (use for WPA, or use as key for WEP)


int keyIndex = 0;            // your network key Index number (needed only for WEP)

unsigned int localPort = 1234;      // local port to listen on

char packetBuffer[255]; //buffer to hold incoming packet

WiFiUDP Udp;

String lastPackage = "";

struct Color{
  int Red;
  int Green;
  int Blue;
};

struct Color fadeColors[NUMPIXELS];



void setup() {
  struct Color Black = { 0, 0, 0};
  for(int i = NUMPIXELS - 1; i >= 0; i--){
    fadeColors[i] = Black;
  }
  
  pixels.begin();

  //Initialize serial and wait for port to open:
  Serial.begin(9600);

  // attempt to connect to Wifi network:
  while (status != WL_CONNECTED) {
    status = WiFi.begin(ssid, pass);
    // wait 10 seconds for connection:
    delay(10000);
  }

  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);
  Udp.begin(localPort);
}

void loop() {

  // if there's data available, read a packet
  int packetSize = Udp.parsePacket();
  if (packetSize) {
    // read the packet into packetBufffer
    int len = Udp.read(packetBuffer, 255);
    if (len > 0) {
        packetBuffer[len] = 0;
        Serial.println("LastPackage: " + lastPackage);
      if(lastPackage != packetBuffer){
        lastPackage = packetBuffer; 
        char delimiter[] = ".";
    
        char cRed[3] = "  ";
        char cGreen[3] = "  ";
        char cBlue[3] = "  ";
    
        strtok(packetBuffer, delimiter);
        strcpy(cRed, strtok(NULL, delimiter));
        int iRed = atoi(cRed);
        strcpy(cGreen, strtok(NULL, delimiter));
        int iGreen = atoi(cGreen);
        strcpy(cBlue, strtok(NULL, delimiter));
        int iBlue = atoi(cBlue);    

        Serial.print(packetBuffer);
        Serial.println("test");
        if(packetBuffer[0] == 'C')  { 
          changeColor(iRed, iGreen, iBlue);
        } else {
          changeColorFade(iRed, iGreen, iBlue);
        }
      }
    }
  }
}


void changeColor(int Red, int Green, int Blue) {
  //pixels.setBrightness(500);
  for (int i = 0; i < NUMPIXELS; i++) {
    pixels.setPixelColor(i, pixels.Color(Red, Green, Blue)); // Moderately bright green color.
    pixels.show(); // This sends the updated pixel color to the hardware.
    delay(delayval); // Delay for a period of time (in milliseconds).
  }
}

void changeColorFade(int Red, int Green, int Blue) {
  int length = sizeof(fadeColors)/sizeof(Color);
  for(int i = NUMPIXELS -1; i > 0; i--){
    fadeColors[i] = fadeColors[i - 1];
  }
  fadeColors[0].Red  = Red;
  fadeColors[0].Green  = Green;
  fadeColors[0].Blue  = Blue;
  Serial.println("ColorsFaded");

  
  //pixels.setBrightness(500);
  for (int i = 0; i < NUMPIXELS; i++) {
    pixels.setPixelColor(i, pixels.Color(fadeColors[i].Red, fadeColors[i].Green, fadeColors[i].Blue)); // Moderately bright green color.
  }
  pixels.show(); // This sends the updated pixel color to the hardware.
}
